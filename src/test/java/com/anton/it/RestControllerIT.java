package com.anton.it;

import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

public class RestControllerIT {


    private Client client;
    private WebTarget target;

    @Before
    public void before() throws Exception {
        Properties props = new Properties();

        String port = System.getProperty("wildfly.remote.port");
        if (port != null) {
            props.setProperty("remote.connection.default.port", port);
        }
        System.out.println("***PORT = " + port + "***");

        client = ClientBuilder.newClient();
//        target = client.target("http://localhost:" + port + "/rest");
        target = client.target("http://localhost:8080/tests/rest");
    }


    @Test
    public void test() {
        System.out.println("***THIS IS A INTEGRATION TEST***");
        String result = target.path("/home").request().get(String.class);

        assertThat(result).isEqualTo("hello");
    }

}
