package com.anton.it;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * @author ahj
 */
@Singleton
@Path("/")
public class RestController {

    @GET
    @Path("/home")
    public String home(){
        return "hello";
    }
}
